# SquidGuard Docker

A docker image based on ubuntu/squid plus squidGuard and automatic blacklists update

# How to

docker pull blys/squidguard

docker run --rm -e TZ=CET -p 3128:3128 blys/squidguard

## as a service in swarm mode:

docker service create --name squidGuard --replicas 1 -e TZ=CET -p 3128:3128 blys/squidguard:latest
