FROM ubuntu/squid:latest

LABEL maintainer="Gianluigi Belli <gianluigi.belli@blys.it>"  name="SquidGuard" description="Ubuntu based Squid proxy docker image integrated with SquidGuard"

## Path to blacklist DBs
ENV BLDB_PATH=/var/lib/squidguard/db/BL

## Install required packages
RUN apt-get update && apt-get install -y \
    squidguard \
    rsync \
 && rm -rf /var/lib/apt/lists/*

## Copy required files
COPY etc/squid/squid.conf /etc/squid/squid.conf
COPY etc/squidguard/squidGuard.conf /etc/squidguard/squidGuard.conf
COPY etc/crontab/keepme.cron /etc/cron.d/keepme
COPY scripts/update-blacklists.sh /usr/local/bin/update-blacklists.sh
COPY scripts/entrypoint.sh /usr/local/bin/entrypoint.sh


## Make scripts executable
RUN chmod +x /usr/local/bin/update-blacklists.sh \
 && chmod +x /usr/local/bin/entrypoint.sh

## Upgrade blacklists
RUN /usr/local/bin/update-blacklists.sh