#!/usr/bin/bash
if test -f "$HOME/.env"; then
    export $(egrep -v '^#' $HOME/.env | xargs)
fi    
echo Fetching updated blacklists
rsync -av rsync://ftp.ut-capitole.fr/blacklist/dest/adult $BLDB_PATH
echo Updating squidGuard dbs
squidGuard  -C all
chown -R proxy:proxy $BLDB_PATH
if test -f "/run/squid.pid"; then
    echo Reload configuaration
    squid -k reconfigure
fi